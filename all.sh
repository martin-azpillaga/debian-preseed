#!/bin/bash
sudo echo "martin ALL=(ALL:ALL) ALL" > /etc/sudoers.d/martin;
sudo apt update;
sudo apt install -y git git-lfs postgresql curl chromium nodejs npm python3 default-jdk docker virt-manager fonts-firacode audacity gimp inkscape krita blender kdenlive obs-studio snapd;
sudo npm install --global serve;
sudo snap install codium --classic;

su martin;
git config --global user.name "Martin Azpillaga";
git config --global user.email "martin@minim.tools";
git config --global core.autocrlf false;
git config --global core.eol lf;
git config --global init.defaultBranch main;
ssh-keygen -q -f $HOME/.ssh/rsa_id.pub -N "";

echo "create role martin superuser login; create database martin;" | sudo -u postgres psql;

read -s -p "Enter your GitLab access token: " ACCESS_TOKEN
cd;
git init;
git remote add gitlab https://gitlab.com/martin-azpillaga/me;
git branch -u gitlab/main;
git pull;
curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" --data-urlencode "title=Automatic key" --data-urlencode "key=$(cat ~/.ssh/id_rsa.pub)" "https://gitlab.example.com/api/v4/user/keys"
git submodule init;
git submodule update;